##
# Lukas Wojciechowski's Portfolio

<p align="center">
  <img src="https://avatars.githubusercontent.com/u/9919?v=4" width="110" alt="Profile Picture">
</p>

## Hello :)

My name is Łukasz Wojciechowski. I was born in Poland and graduated as an Electrotechnician from HTL. In 2015, I moved to Austria; my biggest dream at the time was to learn German in order to function normally in my new "home". In 2022, I decided to change my life and learn a new profession, namely to become a programmer. At that time, I still didn't know where to start, which language to choose, etc. By chance, I came across a course in the basics of Python. After that, things went downhill. I learned about a school called Codersbay Vienna where one could enroll in a two-year Ausbildung in Application Development & Coding. I decided to do everything to take advantage of such a life opportunity. During the first year, I learned Java and the Spring Boot framework, Javascript, HTML, CSS, the Vue.js framework, Kotlin, Android Studio, SQL, PHP and the Laravel framework, as well as the Postman tool. In addition to programming languages, I also learned about data technology, how computer networks and the internet work, the various number systems, how hardware functions, etc.
<p align="center">
  <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRCbdRP9Bj80u53Aw-Vi3sBwVlDO6jNWpb9WWFx5WJVnA&s" width="110" alt="Codersbay Logo">
</p>

## Projects

### [TAZEH](https://gitlab.com/Lukas737/tazeh-2.0)
### [TAZEH -Video presentation on Youtube](https://youtu.be/qXwNjdERivI?si=kKj36o5ssHbwdtag)

I am especially proud of my application ["TAZEH"](https://gitlab.com/Lukas737/tazeh-2.0) written in Android Studio as a project at the end of the second semester, which taught me a lot because, in addition to Kotlin and Jetpack Compose, I used an API connection from OpenAI, and also utilized services offered by Firebase for user registration and login, as well as a NoSQL database for storing tokens.
## 
### [SMApp (Social Media App)](https://gitlab.com/Lukas737/smapp-php-laravel)
### [SMApp - Video presentation on Youtube](https://www.youtube.com/watch?v=k5TDvOllZ7E&t=46s)

SMApp (Social Media App) is an innovative social media application that enables users to connect with others who share similar passions and interests. This project includes various functionalities such as user profiles, post publishing, following users, creating events, browsing events using a swipe mechanism, and more. Users receive notifications for event join requests and reminders for upcoming events. The application was built using the Laravel framework and demonstrates my ability to create complex, user-focused applications.

##

### [NewsApp](https://gitlab.com/Lukas737/newsapp-android)
### [NewsApp - Video presentation on YouTube](https://youtu.be/n7ywafC3yVE?si=6rL6g9pjilRU_k2g)

NewsApp is an Android mobile application built using Jetpack Compose that allows users to browse the latest news from various countries. The app utilizes the NewsAPI to fetch the freshest news articles from selected countries. This project showcases my skills in using modern Android development tools and libraries, ensuring a responsive and intuitive user experience.

##
### [Coderscape - JAVA JMonkeyEngine](https://gitlab.com/Hinotori91/coderscape)

Coderscape is a 3D-coop puzzle platformer game developed during the third semester at CODERS.BAY. The game features custom designed levels where players embark on an adventure to find the missing pet turret of the local blacksmith. This project showcases my skills in game development and collaboration within a team of developers. The project is developed in Java using the JMonkeyEngine.

##
### [FIA-SPS](https://gitlab.com/Branislav95/fia-sps)

Fia-sps is a project developed using Laravel that aims to create a platform for managing an entire school, including schedules, trainers, groups, and students. This project was a collaborative effort by our team at Codersbay Vienna, showcasing our ability to work together on a complex application for our school.

##
